import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { IReceipt, Receipt } from 'app/shared/model/receipt.model';
import { ReceiptService } from './receipt.service';

@Component({
  selector: 'jhi-receipt-update',
  templateUrl: './receipt-update.component.html'
})
export class ReceiptUpdateComponent implements OnInit {
  isSaving: boolean;
  purchaseDateDp: any;

  editForm = this.fb.group({
    id: [],
    storeId: [],
    purchaseDate: [],
    productName: [],
    productPrice: [],
    productAmount: [],
    totalPrice: []
  });

  constructor(protected receiptService: ReceiptService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ receipt }) => {
      this.updateForm(receipt);
    });
  }

  updateForm(receipt: IReceipt) {
    this.editForm.patchValue({
      id: receipt.id,
      storeId: receipt.storeId,
      purchaseDate: receipt.purchaseDate,
      productName: receipt.productName,
      productPrice: receipt.productPrice,
      productAmount: receipt.productAmount,
      totalPrice: receipt.totalPrice
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const receipt = this.createFromForm();
    if (receipt.id !== undefined) {
      this.subscribeToSaveResponse(this.receiptService.update(receipt));
    } else {
      this.subscribeToSaveResponse(this.receiptService.create(receipt));
    }
  }

  private createFromForm(): IReceipt {
    return {
      ...new Receipt(),
      id: this.editForm.get(['id']).value,
      storeId: this.editForm.get(['storeId']).value,
      purchaseDate: this.editForm.get(['purchaseDate']).value,
      productName: this.editForm.get(['productName']).value,
      productPrice: this.editForm.get(['productPrice']).value,
      productAmount: this.editForm.get(['productAmount']).value,
      totalPrice: this.editForm.get(['totalPrice']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IReceipt>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
