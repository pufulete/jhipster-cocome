export interface IProduct {
  id?: number;
  barcode?: number;
  purchasePrice?: number;
  name?: string;
}

export class Product implements IProduct {
  constructor(public id?: number, public barcode?: number, public purchasePrice?: number, public name?: string) {}
}
