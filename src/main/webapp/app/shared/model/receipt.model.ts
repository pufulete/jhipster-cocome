import { Moment } from 'moment';

export interface IReceipt {
  id?: number;
  storeId?: number;
  purchaseDate?: Moment;
  productName?: string;
  productPrice?: number;
  productAmount?: number;
  totalPrice?: number;
}

export class Receipt implements IReceipt {
  constructor(
    public id?: number,
    public storeId?: number,
    public purchaseDate?: Moment,
    public productName?: string,
    public productPrice?: number,
    public productAmount?: number,
    public totalPrice?: number
  ) {}
}
