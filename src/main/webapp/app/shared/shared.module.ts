import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CocoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [CocoSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [CocoSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CocoSharedModule {
  static forRoot() {
    return {
      ngModule: CocoSharedModule
    };
  }
}
