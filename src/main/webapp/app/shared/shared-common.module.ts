import { NgModule } from '@angular/core';

import { CocoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
  imports: [CocoSharedLibsModule],
  declarations: [JhiAlertComponent, JhiAlertErrorComponent],
  exports: [CocoSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class CocoSharedCommonModule {}
