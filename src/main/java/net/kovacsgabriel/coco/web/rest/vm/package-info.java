/**
 * View Models used by Spring MVC REST controllers.
 */
package net.kovacsgabriel.coco.web.rest.vm;
